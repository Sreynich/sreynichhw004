package com.example.fragementhw;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class EmailActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<ListData> listsData = new ArrayList<>();
    ListAdapter listAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_email_layout);
        recyclerView = findViewById(R.id.recycler_view);

        for (int i = 0 ; i<30; i++){

            listsData.add(new ListData("sreynichhorn"+i+"@gmail.com"));
        }
        listAdapter = new ListAdapter(listsData);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(listAdapter);
    }
}
