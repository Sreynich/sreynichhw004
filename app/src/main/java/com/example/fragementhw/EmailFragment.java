package com.example.fragementhw;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class EmailFragment extends Fragment {

    RecyclerView recyclerView;
    ListAdapter listAdapter;
    ArrayList<ListData> listsData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_email_layout, container, false);

        recyclerView = view.findViewById(R.id.recycler_view);

        for (int i = 0 ; i<30; i++){

            listsData.add(new ListData("sreynichhorn"+i+"@gmail.com"));
        }
        listAdapter = new ListAdapter(listsData);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(listAdapter);
    return view;
    }

}
