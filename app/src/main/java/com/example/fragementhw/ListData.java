package com.example.fragementhw;


public class ListData {

    private String email;

    public ListData(String email) {

        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
